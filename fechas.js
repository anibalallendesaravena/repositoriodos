const moment = require('moment');
moment.locale('es');
​
const horaLocal = moment().format('h:mm:ss a'); 
console.log(`Hora local: ${horaLocal}`);
​
const utc = moment.utc().format('h:mm:ss a');
console.log(`Hora UTC: ${utc}`);
​
const localEnhoras = moment().hour();
const utcEnHoras = moment.utc().hour();
const diferenciaEnHoras = (utcEnHoras + 24 - localEnhoras) % 24;
console.log(`Diferencia horaria: ${diferenciaEnHoras} horas`);
​
console.log('Hola automatico después de guardar')